import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { authSelector, setUser } from '@/store/slices/authSlice';
import { auth } from '@/dal';
import { useRegistrationStep } from '@/hooks';
import { Routing } from '@/components/features';
import { Layout } from './components/common/layout';
import GlobalFonts from './assets/fonts/ApplyGlobalFonts';
import {
    LandingView,
    LoginView,
    ConfirmEmail,
    RegisterView,
    ConfirmView,
    ChoosePlanView,
    DashboardView,
    DicomViewerView,
    AuditView,
} from './views';

const { ProtectedRouting } = Routing;

const App: React.FC = () => {
    const { user } = useSelector(authSelector);
    useRegistrationStep({ user });
    const dispatch = useDispatch();

    useEffect(() => {
        auth.getMe().then(res => {
            dispatch(setUser(res));
        });
    }, []);

    const { id } = user;

    return (
        <>
            <GlobalFonts />
            <Switch>
                <Route path="/confirm-email" component={ConfirmEmail} />
                <ProtectedRouting path="/login" component={LoginView} open={!id} />
                <ProtectedRouting path="/register" component={RegisterView} open={!id} />
                <Route path="/confirm" component={ConfirmView} />
                <ProtectedRouting path="/choose-plan" component={ChoosePlanView} open={!!id} />
                <Layout>
                    <ProtectedRouting path="/dashboard" component={DashboardView} open={!!id} />
                    <ProtectedRouting exact path="/viewer" component={DicomViewerView} open={!!id} />
                    <ProtectedRouting exact path="/audit/:type" component={AuditView} open={!!id} />
                    <Route exact path="/" component={LandingView} />
                </Layout>
            </Switch>
        </>
    );
};

export default App;
