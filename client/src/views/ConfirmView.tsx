import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useQueryString } from '@/hooks';
import { emailConfirmation } from '@/dal';
import { AxiosError } from 'axios';
import styled from 'styled-components';
import { COLORS } from '@/constants';
import { Spin } from 'antd';
import { card, Space, button, container } from '@/components/common';
import Email from '@/assets/images/email.svg';

const { StatusCard } = card;
const { PrimaryButton } = button;
const { BackgroundColorContainer } = container;

const EmailImg = styled.img`
    width: 50%;
`;

const ConfirmView: React.FC = () => {
    const [message, setMessage] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const queryString = useQueryString();

    useEffect(() => {
        setIsLoading(true);
        const token = queryString.params.get('token');

        emailConfirmation
            .confirm(token || '')
            .catch((err: AxiosError) => setMessage(err.response?.data.message || ''))
            .finally(() => setIsLoading(false));
    }, []);
    return (
        <BackgroundColorContainer color={COLORS[2]}>
            {isLoading ? (
                <Spin size="large" spinning={isLoading} />
            ) : (
                <>
                    <StatusCard type={message ? 'error' : 'success'} title={message || 'Success'}>
                        <Space direction="row" alignItems="center" justifyContent="space-around">
                            <EmailImg src={Email} />
                            {!message ? (
                                <Link to="/login">
                                    <PrimaryButton text="Login" />
                                </Link>
                            ) : (
                                <></>
                            )}
                        </Space>
                    </StatusCard>
                </>
            )}
        </BackgroundColorContainer>
    );
};

export default ConfirmView;
