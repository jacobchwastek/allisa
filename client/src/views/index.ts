import LandingView from './LandingView';
import LoginView from './LoginView';
import RegisterView from './RegisterView';
import ConfirmEmail from './ConfirmEmail';
import ConfirmView from './ConfirmView';
import ChoosePlanView from './ChoosePlanView';
import DashboardView from './DashboardView';
import DicomViewerView from './DicomViewerView';
import AuditView from './AuditView';

export {
    LandingView,
    LoginView,
    RegisterView,
    ConfirmEmail,
    ConfirmView,
    ChoosePlanView,
    DashboardView,
    DicomViewerView,
    AuditView,
};
