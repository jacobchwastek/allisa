import React, { useEffect, useState } from 'react';
import { useLocation, useRouteMatch } from 'react-router-dom';
import { container } from '@/components/common';
import { Audit } from '@/components/features';

const { DefaultContainer } = container;
const { BrowserAudit } = Audit;

const AuditView: React.VFC = () => {
    const [auditType, setAuditType] = useState('');
    const location = useLocation();
    const match = useRouteMatch();

    useEffect(() => {
        const { params } = match as any;
        setAuditType(params?.type);
    }, [location.pathname]);

    const displayTypes = () => {
        switch (auditType) {
            case 'browser':
                return <BrowserAudit />;

            default:
                return <></>;
        }
    };

    return <DefaultContainer style={{ justifyContent: 'center' }}>{displayTypes()}</DefaultContainer>;
};

export default AuditView;
