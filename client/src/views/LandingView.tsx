import React from 'react';
import { container } from '@/components/common';
import { views } from '../components';

const { DefaultContainer } = container;

const {
    Landing: { DoctorBlob, TextSection },
} = views;

const LandingView: React.FC = () => (
    <DefaultContainer>
        <DoctorBlob />
        <TextSection />
    </DefaultContainer>
);

export default LandingView;
