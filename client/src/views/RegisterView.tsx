import React from 'react';
import { Row, Col } from 'antd';
import { Auth } from '@/components/views';
import { Content } from '@/components/common';

const { RegisterForm, AuthSider, AuthContainer } = Auth;

const RegisterView: React.FC = () => (
    <AuthContainer>
        <Row gutter={[16, 16]} className="register-view-row--desktop">
            <Col span={18}>
                <Content>
                    <RegisterForm />
                </Content>
            </Col>
            <Col span={6}>
                <AuthSider />
            </Col>
        </Row>
        <Row gutter={[16, 16]} className="auth register-view-row--mobile">
            <AuthSider mobile />
        </Row>
        <Row gutter={[16, 16]} className="form register-view-row--mobile">
            <Content>
                <RegisterForm />
            </Content>
        </Row>
    </AuthContainer>
);

export default RegisterView;
