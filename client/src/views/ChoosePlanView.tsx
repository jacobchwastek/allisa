import React, { useState } from 'react';
import { container, Space, text } from '@/components/common';
import { PricingPlan } from '@/components/features';
import { useDispatch } from 'react-redux';
import { Button } from 'antd';
import { logOut } from '@/store/slices/authSlice';

const { PricingPlanCard, PricingPlanGroupContainer } = PricingPlan;
const { BackgroundColorContainer } = container;
const { Title } = text;

const ChoosePlanView: React.FC = () => {
    const [plans, setPlans] = useState([]);
    const dispatch = useDispatch();
    return (
        <BackgroundColorContainer direction="row">
            <Space direction="column">
                <Title>Pricing Plans</Title>
                <PricingPlanGroupContainer>
                    <PricingPlanCard
                        onChoose={() => {}}
                        price={20}
                        src="https://s22.postimg.cc/8mv5gn7w1/paper-plane.png"
                    />
                    <PricingPlanCard onChoose={() => {}} price={20} src="https://s28.postimg.cc/ju5bnc3x9/plane.png" />
                    <PricingPlanCard
                        onChoose={() => {}}
                        price={20}
                        src="https://s21.postimg.cc/tpm0cge4n/space-ship.png"
                    />
                </PricingPlanGroupContainer>
                <Title style={{ color: 'white', marginTop: '10px' }} level={2}>
                    Not ready yet?
                </Title>
                <Button type="text" onClick={() => dispatch(logOut())}>
                    Logout
                </Button>
            </Space>
        </BackgroundColorContainer>
    );
};

export default ChoosePlanView;
