import React from 'react';
import styled from 'styled-components';
import { DwvComponent } from '@/components/features/DvwReact';

const Container = styled.div`
    flex: 1;
    background-color: #bdd4e7;
    background-image: linear-gradient(315deg, #bdd4e7 0%, #8693ab 74%);
`;

const DicomViewerView = () => (
    <Container>
        <DwvComponent />
    </Container>
);

export default DicomViewerView;
