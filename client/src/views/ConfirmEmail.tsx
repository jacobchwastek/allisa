import React from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import { Card, Divider, Button } from 'antd';
import Email from '@/assets/images/email.svg';
import { common } from '@/components';
import { useSelector, useDispatch } from 'react-redux';
import { authSelector, logOut } from '@/store/slices/authSlice';

const {
    Space,
    text: { Title },
} = common;

const Container = styled.div`
    height: 100vh;
    background-color: ${({ theme }) => theme.lightBlue};
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;

const StyledCard = styled(Card)`
    .ant-card-head {
        border: none;
    }
    .ant-card-head-title {
        display: flex;
        justify-content: center;
        img {
            width: 20%;
        }
    }
    .ant-card-extra {
        align-self: baseline;
    }
`;

const ConfirmEmail: React.FC = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { user } = useSelector(authSelector);

    return (
        <Container className="confirm-email-container">
            <StyledCard
                title={<img src={Email} alt="email-cover" />}
                extra={
                    <Button
                        onClick={() => {
                            dispatch(logOut());
                            history.push('/');
                        }}
                        type="default">
                        Log out
                    </Button>
                }>
                <Space direction="column" alignItems="center">
                    <Title>Email confirmation</Title>
                    <h2>
                        We have sent email to
                        {` ${user.email} `}
                        to confirm the validity of your email address. After receiving the email follow the link
                        provided to complete your registration.
                    </h2>
                    <Divider />
                    <h3>
                        If you have not received the email, please check your Spam or Trash folder or click resend
                        button
                    </h3>
                    <Button type="link">Resend confirmation email</Button>
                </Space>
            </StyledCard>
        </Container>
    );
};

export default ConfirmEmail;
