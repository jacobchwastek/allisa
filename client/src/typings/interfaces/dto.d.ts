interface ICreateVisitorAudit {
    id: string;
    resolutionX: number;
    resolutionY: number;
    browserName: string;
    browserVersion: string;
    oS: string;
}
