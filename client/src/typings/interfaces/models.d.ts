type IAccountSettings = {
    id: string;
    registrationStep: number;
    isConfirmed: boolean;
};

interface IUser {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    accountSettings: IAccountSettings;
    role: string;
}

interface ILogin {
    email: string;
    password: string;
}

interface IError {
    message: string;
    code: number;
}

interface AuthError {
    message: string;
    code: number;
}

interface AuthState {
    isAuth: boolean;
    user: IUser;
    isLoading: boolean;
    error: AuthError;
}

interface IVisitorAudit {
    id: string;
    resolutionX: number;
    resolutionY: number;
    browserName: string;
    browserVersion: string;
    oS: string;
    createdAt: Date;
    user: IUser;
}
