// import original module declaration
import 'styled-components';

// and extend it
declare module 'styled-components' {
    export interface DefaultTheme {
        darkBlue: string;
        lightBlue: string;
        lightGreen: string;
        white: string;
        light: string;
        red: string;
    }
}
