const DEFAULT_USER: IUser = {
    email: '',
    firstName: '',
    id: '',
    lastName: '',
    password: '',
    accountSettings: {
        id: '',
        isConfirmed: false,
        registrationStep: 0,
    },
};

export { DEFAULT_USER };
