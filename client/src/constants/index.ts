import COLORS from './colors.json';
import DICTIONARIES from './dictionaries.json';
import * as DEFAULT_TYPES from './defaultTypes';

export { COLORS, DEFAULT_TYPES, DICTIONARIES };
