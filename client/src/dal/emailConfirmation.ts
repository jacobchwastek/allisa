import { api } from '../utils/api';

const confirm = async (token: string) => api.post('/mail/confirm', { token }).then(res => res.data);

export { confirm };
