import { browser, api } from '@/utils/';

export const auditBrowserDetails = async () => {
    const details = browser.getBrowserDetails();
    const {
        screen: { availHeight, availWidth },
    } = window;
    await api.post('/audit/browser', { ...details, resolutionX: availWidth, resolutionY: availHeight });
};

export const getAuditBrowserDetails = () => api.get('/audit/browser');
