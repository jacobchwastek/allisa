import React, { ReactChildren, ReactChild } from 'react';
import styled from 'styled-components';

type Props = {
    children: ReactChild | ReactChildren | ReactChild[] | ReactChildren[];
};

const AuthContainer: React.FC<Props> = ({ children }: Props) => (
    <Container className="auth-container">{children}</Container>
);

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;

    @media screen and (max-width: 992px) {
        flex-direction: column;
        .register-view-row--desktop {
            display: none;
        }
        .form {
            height: 100%;
        }
        .ant-row {
            .ant-col {
                padding: 0 !important;
            }
        }
    }
    @media screen and (min-width: 993px) {
        .register-view-row--mobile {
            display: none;
        }
        .ant-row {
            flex: 1;
            .ant-col {
                padding: 0 !important;
            }
        }
    }
`;

export default AuthContainer;
