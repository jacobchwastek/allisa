import React, { useState, useEffect, memo } from 'react';
import { useHistory } from 'react-router-dom';
import { Form, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { slices } from '@/store';
import { interceptors } from '@/utils';
import { Input, button } from '@/components/common';
import { Auth } from '@/components/views';

const { PrimaryButton } = button;
const {
    authSlice: { login, authSelector },
} = slices;

const LoginForm: React.FC = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const [errorStatus, setErrorStatus] = useState<'success' | 'error'>('success');
    const [form] = Form.useForm();
    const { error, isAuth } = useSelector(authSelector);

    useEffect(() => {
        const handleAfterSubmitError = () => {
            const { code } = error;
            if (code === 200) return true;
            if (code === 401) {
                message.warn('Invalid email address or password');
                return setErrorStatus('error');
            }
            return interceptors.interceptPostError('Incorrect email or password', code);
        };
        handleAfterSubmitError();
    }, [error]);

    useEffect(() => {
        if (isAuth) {
            message.success('Success');
            form.resetFields();
            history.push('/dashboard');
        }
    }, [isAuth]);

    const onFinish = (values: any) => {
        dispatch(login({ ...values }));
    };

    const onInputError = () => {
        if (errorStatus === 'error') setErrorStatus('success');
    };

    return (
        <Auth.AuthCard title="Login" bordered={false}>
            <Form form={form} name="login" onFinish={onFinish} initialValues={{ remember: true }}>
                <Form.Item
                    name="email"
                    validateStatus={errorStatus}
                    rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                    ]}>
                    <Input onClick={onInputError} placeholder="Email" prefix={<UserOutlined />} />
                </Form.Item>
                <Form.Item
                    name="password"
                    validateStatus={errorStatus}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                    hasFeedback>
                    <Input.Password prefix={<LockOutlined />} onClick={onInputError} placeholder="Password" />
                </Form.Item>
                <Form.Item className="form-item-button">
                    <PrimaryButton text="Login" htmlType="submit" />
                </Form.Item>
            </Form>
        </Auth.AuthCard>
    );
};

export default memo(LoginForm);
