import React from 'react';
import styled from 'styled-components';
import Logo from '../../../assets/images/logo.png';

type Props = {
    mobile?: boolean;
};

const AuthSider: React.FC = ({ mobile }: Props) => (
    <Container mobile={mobile} className="auth-sider-container">
        <Img mobile={mobile} src={Logo} alt="" />
        <>
            {!mobile && (
                <TextContainer>
                    <Text>Welcome</Text>
                    <Text>to </Text>
                    <Text>Alissa Med</Text>
                </TextContainer>
            )}
        </>
    </Container>
);

AuthSider.defaultProps = {
    mobile: false,
};

const Container = styled.div<Props>`
    width: 100%;
    background-color: ${({ theme }) => theme.lightBlue};
    height: ${({ mobile }) => (mobile ? '100%' : '100vh')};
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    align-items: center;
    padding: ${({ mobile }) => (mobile ? '2% 0 2% 0' : '4% 1% 4% 1%')};
`;

const Img = styled.img<Props>`
    width: ${props => (props.mobile ? '10%' : '40%')};
`;

const TextContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

const Text = styled.h1`
    font-family: Gudea;
    font-style: normal;
    font-weight: normal;
    font-size: 48px;
    line-height: 59px;
    display: flex;
    align-items: center;
    text-align: center;
    text-transform: uppercase;
    color: #ffffff;
`;

export default AuthSider;
