import React from 'react';
import styled from 'styled-components';
import DoctorImg from '../../../assets/images/doctor-vector.svg';

const DoctorBlob: React.VFC = () => <Doctor src={DoctorImg} className="doctor-blob" />;

const Doctor = styled.img`
    flex-grow: 1;
    flex-basis: 0;
    height: 100%;
    width: 90%;
    max-height: 650px;
`;

export default DoctorBlob;
