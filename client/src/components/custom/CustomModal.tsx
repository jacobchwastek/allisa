import { Component } from 'react';
import { Modal } from 'antd';
import { ModalFuncProps, ModalProps } from 'antd/lib/modal';
import styled from 'styled-components';

import { COLORS } from '../../constants';

const StyledModal = styled(Modal)``;

export default class CustomModal extends Component<ModalProps> {
    static displayCustomConfirm = (props: ModalFuncProps): void => {
        Modal.confirm({
            okButtonProps: {
                style: {
                    backgroundColor: COLORS[4],
                    border: 'none',
                },
            },
            cancelButtonProps: {
                type: 'link',
                style: {
                    color: COLORS[1],
                },
            },
            width: 600,
            ...props,
        });
    };

    static displayCustomWarning = (props: ModalFuncProps): void => {
        Modal.warning({
            okButtonProps: {
                style: {
                    backgroundColor: COLORS[4],
                    border: 'none',
                },
            },
            width: 600,
            ...props,
        });
    };

    static displayCustomSuccess = (props: ModalFuncProps): void => {
        Modal.success({
            okButtonProps: {
                style: {
                    backgroundColor: COLORS[4],
                    border: 'none',
                },
            },
            width: 600,
            ...props,
        });
    };

    render = (): JSX.Element => {
        const { children } = this.props;
        return (
            <StyledModal
                width={1000}
                okButtonProps={{
                    style: {
                        backgroundColor: COLORS[4],
                        border: 'none',
                    },
                }}
                cancelButtonProps={{
                    color: COLORS[1],
                }}
                {...this.props}>
                {children}
            </StyledModal>
        );
    };
}
