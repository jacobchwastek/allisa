import React from 'react';
import { Card } from 'antd';
import Logo from '@/assets/images/logo.png';
import styled from 'styled-components';
import Space from '../Space';

type Props = {
    children?: React.ReactChild | React.ReactChild[];
    type: 'success' | 'error';
    title?: string;
};

type StyleProps = {
    status: 'success' | 'error';
};

const StyledCard = styled(Card)<StyleProps>`
    width: 50%;

    .ant-card-head {
        background-color: ${({ status, theme }) => (status === 'success' ? `${theme.lightGreen}` : `${theme.red}`)};
        border: none;
        .space {
            min-height: 110px;
        }
    }
`;

const LogoImg = styled.img`
    width: 10%;
    position: absolute;
`;

const StatusCard: React.FC<Props> = ({ children, type, title = '' }: Props) => (
    <StyledCard
        status={type}
        title={
            <Space alignItems="center">
                <LogoImg src={Logo} />
                <Space justifyContent="center">
                    <h1>{title}</h1>
                </Space>
            </Space>
        }>
        {children}
    </StyledCard>
);

StatusCard.defaultProps = {
    children: <></>,
    title: '',
};
export default StatusCard;
