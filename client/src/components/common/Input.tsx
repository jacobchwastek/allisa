import React from 'react';
import { Input } from 'antd';
import styled from 'styled-components';

const StyledInput = styled(Input)`
    .ant-input {
        font-family: Gudea;
        font-style: normal;
        font-weight: normal;
        font-size: 18px !important;
        line-height: 24px;
    }
    .ant-input-prefix {
        font-size: 18px;
    }
    height: 60px;
`;

const StyledInputPassword = styled(Input.Password)`
    .ant-input {
        font-family: Gudea;
        font-style: normal;
        font-weight: normal;
        font-size: 18px !important;
        line-height: 24px;
    }
    .ant-input-prefix {
        font-size: 18px;
    }
    height: 60px;
`;

StyledInput.Password = StyledInputPassword;

export default StyledInput;

export { StyledInput, StyledInputPassword };
