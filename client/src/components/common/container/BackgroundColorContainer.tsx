import React from 'react';
import styled from 'styled-components';

type Props = {
    children?: React.ReactChild | React.ReactChild[];
    direction?: FlexDirection;
    color?: string;
    align?: AlignItems;
    justify?: JustifyContent;
};

type StyleProps = {
    direction: FlexDirection;
    color?: string;
    align: AlignItems;
    justify: JustifyContent;
};

const Container = styled.div<StyleProps>`
    min-height: 100vh;
    background-color: ${({ theme, color }) => color || theme.lightBlue};
    display: flex;
    flex-direction: ${props => props.direction};
    align-items: center;
    flex: 1;
    justify-content: center;
`;

const BackgroundColorContainer: React.FC<Props> = ({
    children,
    color = undefined,
    direction = 'row',
    align = 'center',
    justify = 'center',
}: Props) => (
    <Container
        align={align}
        justify={justify}
        direction={direction}
        className="background-color-flex-container"
        color={color}>
        {children}
    </Container>
);

BackgroundColorContainer.defaultProps = {
    children: <></>,
    direction: 'row',
    color: undefined,
    align: 'center',
    justify: 'center',
};

export default BackgroundColorContainer;
