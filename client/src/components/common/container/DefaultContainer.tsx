import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    padding-top: 100px;
    min-height: 100vh;
    flex-wrap: wrap;
    background-color: #7f5a83;
    background-image: linear-gradient(315deg, #7f5a83 0%, #0d324d 74%);
    display: flex;
    flex-direction: row;
    align-items: center;

    @media screen and (max-width: 1400px) {
        flex-direction: column;
        .landing-text-section {
            margin-left: 30%;
        }
    }
`;

export default Container;
