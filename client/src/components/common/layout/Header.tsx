import React, { useState } from 'react';
import styled from 'styled-components';
import { Layout as ALayout } from 'antd';
import { useHistory, Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { authSelector, logOut } from '@/store/slices/authSlice';
import logo from '../../../assets/images/logo.png';
import { useScroll } from '../../../hooks';

type Props = {
    isVisible?: boolean;
};

const Header: React.FC<Props> = ({ isVisible }: Props) => {
    const history = useHistory();
    const [transform, setTransform] = useState(false);
    const { isAuth } = useSelector(authSelector);
    const dispatch = useDispatch();
    const onLogoClick = () => history.push('/');

    useScroll(() => {
        const pos = window.scrollY;

        if (pos === 0) setTransform(false);
        if (pos >= 10) setTransform(true);
    });

    return (
        <StyledHeader
            visible={isVisible}
            transform={`${transform}`}
            style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
            <div className="logo-container">
                <img aria-hidden="true" onClick={onLogoClick} src={logo} alt="logo" />
                <h1>Allisa Med</h1>
            </div>
            <div className="menu">
                <Link className="menu-link" to="/faq">
                    <p>FAQ</p>
                </Link>
                <Link className="menu-link" to="/pricing">
                    <p>Pricing</p>
                </Link>
                {isAuth ? (
                    <button type="button" onClick={() => dispatch(logOut())}>
                        <p>Logout</p>
                    </button>
                ) : (
                    <>
                        <Link className="menu-link" to="/login">
                            <p>Login</p>
                        </Link>
                        <Link className="menu-link" to="/register">
                            <p>Register</p>
                        </Link>
                    </>
                )}
            </div>
        </StyledHeader>
    );
};

Header.defaultProps = {
    isVisible: true,
};

type StyledProps = {
    transform: string;
    visible?: boolean;
};

const StyledHeader = styled(ALayout.Header)<StyledProps>`
    display: ${props => (props?.visible ? 'flex' : 'none')};
    background: rgba(255, 255, 255, 0.05) !important;
    box-shadow: 0px 4px 4px rgb(0 0 0 / 25%);
    z-index: 2 !important;
    justify-content: space-between;
    background-color: ${props => (props.transform ? 'rgba(0, 21, 41, 0.85)' : 'rgba(0,0,0,0)')};
    transition: 1s;
    height: ${props => (props.transform ? '64px' : '70px')};

    .menu {
        justify-content: space-evenly;
        display: flex;

        .menu-link,
        button {
            text-decoration: none;
            color: white;
            width: 200px;
            justify-content: center;
            font-family: Gudea;
            font-style: normal;
            font-weight: normal;
            font-size: 20px;
            line-height: 30px;
            display: flex;
            align-items: center;
            padding: 10px 0 10px 0;
            background-color: transparent;
            border: none;
            p {
                margin: 0;
                &:hover {
                    border-bottom: 2px solid #ffffff;
                }
            }
        }
    }

    .logo-container {
        float: left;
        width: 300px;
        height: 100%;
        padding: 10px 0;
        display: flex;
        align-items: center;
        justify-content: space-between;

        h1 {
            font-style: normal;
            font-weight: normal;
            margin: 0;
            color: #ffffff;
            font-family: 'Gudea';
            font-size: 22px;
            transition: 1s;
        }

        img {
            height: 100%;
            transition: transform 0.8s ease-in-out;

            &:hover {
                transform: rotate(360deg);
            }
        }
    }
`;

export default Header;
