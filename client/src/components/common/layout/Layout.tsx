import React from 'react';
import { Layout as ALayout } from 'antd';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { authSelector } from '@/store/slices/authSlice';
import { useShouldDisplay } from '@/hooks';
import { Sider, Header } from '.';

type Props = {
    children?: React.ReactChild | React.ReactChild[];
};

const StyledLayout = styled(ALayout)`
    flex: 1;

    #components-layout-demo-side .logo {
        height: 32px;
        margin: 16px;
        background: rgba(255, 255, 255, 0.3);
    }

    .site-layout .site-layout-background {
        background: #fff;
    }
`;

const Layout: React.FC<Props> = ({ children }: Props) => {
    const {
        user: { id },
    } = useSelector(authSelector);
    const locations = ['/', '/confirm', '/dashboard'];
    const { shouldDisplay } = useShouldDisplay({ locations });
    return (
        <StyledLayout>
            {id && <Sider />}
            <ALayout className="site-layout">
                <Header isVisible={shouldDisplay} />
                {children}
            </ALayout>
        </StyledLayout>
    );
};

Layout.defaultProps = {
    children: <></>,
};

export default Layout;
