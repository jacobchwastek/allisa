import React, { useState } from 'react';
import { Layout as ALayout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { SettingOutlined, PlusCircleOutlined, HomeOutlined } from '@ant-design/icons';
import Logo from '@/assets/images/logo.png';
import { authSelector, logOut } from '@/store/slices/authSlice';
import { useDispatch, useSelector } from 'react-redux';
import { button } from '@/components/common';

const { SubMenu } = Menu;
const { Sider: ASider } = ALayout;
const { HoverButton } = button;

type Props = {
    collapsed: boolean;
};

const StyledSider = styled(ASider)<Props>`
    .ant-layout-sider-children {
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    li {
        ${props =>
            props.collapsed &&
            `
              display: flex;
              justify-content: center;
        `}
    }
    .hover-button {
        position: fixed !important;
        bottom: 48px !important;
        z-index: 1;
        font-size: inherit;
        color: #fff;
        line-height: 48px;
        text-align: center;
        cursor: pointer;
    }
`;

const Img = styled.img`
    width: 50%;
    padding: 5%;
`;

const LogoutButton = styled(HoverButton)<Props>``;

const Sider: React.FC = () => {
    const [collapsed, setCollapsed] = useState(false);
    const dispatch = useDispatch();
    const {
        user: { role },
    } = useSelector(authSelector);

    const AdminSider = () => (
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<HomeOutlined />}>
                <Link to="/dashboard">Dashboard</Link>
            </Menu.Item>
            <SubMenu key="sub2" icon={<PlusCircleOutlined />} title="Application">
                <SubMenu key="audit" title="Audit">
                    <Link to="/audit/browser">
                        <Menu.Item key="6">Browser</Menu.Item>
                    </Link>
                </SubMenu>
                <SubMenu key="users" title="Users">
                    <Link to="/users/all">
                        <Menu.Item key="7">All</Menu.Item>
                    </Link>
                </SubMenu>
                <SubMenu key="dictionaries" title="Dictionaries">
                    <Menu.Item key="9">All</Menu.Item>
                </SubMenu>
            </SubMenu>
            <Menu.Item key="3" icon={<SettingOutlined />}>
                <Link to="/account">Settings</Link>
            </Menu.Item>
        </Menu>
    );

    const UserSider = () => (
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<HomeOutlined />}>
                <Link to="/dashboard">Dashboard</Link>
            </Menu.Item>
            <SubMenu key="sub2" icon={<PlusCircleOutlined />} title="Application">
                <Menu.Item key="5">
                    <Link to="/viewer">Dicom Viewer Demo</Link>
                </Menu.Item>
                <Menu.Item key="6">Option 6</Menu.Item>
                <SubMenu key="sub3" title="Submenu">
                    <Menu.Item key="7">Option 7</Menu.Item>
                    <Menu.Item key="8">Option 8</Menu.Item>
                </SubMenu>
            </SubMenu>
            <Menu.Item key="3" icon={<SettingOutlined />}>
                <Link to="/account">Settings</Link>
            </Menu.Item>
        </Menu>
    );

    return (
        <StyledSider collapsible onCollapse={() => setCollapsed(coll => !coll)} collapsed={collapsed}>
            <Img src={Logo} alt="logo" />
            {role === 'admin' ? <AdminSider /> : <UserSider />}
            <LogoutButton
                onClick={() => dispatch(logOut())}
                height="48"
                width={collapsed ? '80' : '200'}
                collapsed={collapsed}>
                Logout
            </LogoutButton>
        </StyledSider>
    );
};

export default Sider;
