import React from 'react';
import styled from 'styled-components';

import { DownCircleOutlined, UpCircleOutlined, LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons';

type Props = {
    direction: 'up' | 'down' | 'left' | 'right';
    onClick?: () => void;
};

const ArrowButton = ({ direction, onClick }: Props) => {
    const getImg = () => {
        switch (direction) {
            case 'down':
                return <DownCircleOutlined />;
            case 'up':
                return <UpCircleOutlined />;
            case 'left':
                return <LeftCircleOutlined />;
            case 'right':
                return <RightCircleOutlined />;
            default:
                return <></>;
        }
    };

    return (
        <Button onClick={() => onClick && onClick()} className="arrow-button">
            {getImg()}
        </Button>
    );
};

ArrowButton.defaultProps = {
    onClick: () => {},
};

const Button = styled.button`
    svg {
        font-size: 60px;
    }
    background-color: rgba(0, 0, 0, 0);
    outline: none;
    width: 80px;
    height: 80px;
    display: flex;
    justify-content: center;
    align-items: center;
    border: none;

    border-radius: 100%;
`;

export default ArrowButton;
