import React from 'react';
import {} from 'antd';
import styled from 'styled-components';

type Props = {
    children?: React.ReactChild | React.ReactChild[];
    width?: string;
    height?: string;
    onClick?: () => void;
};

type StyleProps = {
    width?: string;
    height?: string;
};
const StyledButton = styled.div<StyleProps>`
    width: ${({ width }) => `${width}px` || '130px'};
    height: ${({ height }) => `${height}px` || '65px'};
    margin: 15px auto;
    color: #4274d3;
    font-family: 'Open Sans', sans-serif;
    font-size: 1.15rem;
    line-height: 65px;
    text-transform: uppercase;
    text-align: center;
    position: relative;
    cursor: pointer;
    stroke: #4274d3;
    background: ${({ theme }) => theme.darkBlue};

    svg {
        position: absolute;
        top: 0;
        left: 0;

        rect {
            fill: none;
            stroke: #4274d3;
            stroke-width: 1;
            stroke-dasharray: 400, 0;
            -webkit-transition: all 0.8s ease-in-out;
            -moz-transition: all 0.8s ease-in-out;
            -ms-transition: all 0.8s ease-in-out;
            -o-transition: all 0.8s ease-in-out;
        }
    }

    &:hover svg rect {
        stroke-width: 3;
        stroke-dasharray: 35, 245;
        stroke-dashoffset: 38;
        -webkit-transition: all 0.8s ease-in-out;
        -moz-transition: all 0.8s ease-in-out;
        -ms-transition: all 0.8s ease-in-out;
        -o-transition: all 0.8s ease-in-out;
    }
`;

const HoverButton: React.FC<Props> = ({ children, width, height, onClick }: Props) => {
    const buttonWidth = width || 130;
    const buttonHeight = height || 65;
    return (
        <StyledButton width={width} onClick={onClick} className="hover-button">
            {children}
            <svg
                width={buttonWidth}
                height={buttonHeight}
                viewBox={`0 0 ${buttonWidth} ${buttonHeight}`}
                xmlns="http://www.w3.org/2000/svg">
                <rect x="0" y="0" fill="none" width={buttonWidth} height={buttonHeight} />
            </svg>
        </StyledButton>
    );
};

HoverButton.defaultProps = {
    width: undefined,
    children: <></>,
};

export default HoverButton;
