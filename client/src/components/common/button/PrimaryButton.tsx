import React from 'react';
import styled from 'styled-components';

type Props = {
    text?: string;
    htmlType?: 'button' | 'submit' | 'reset';
    height?: string;
    width?: string;
    font?: string;
    onClick?: () => void;
};

type StyleProps = {
    height: string;
    width: string;
    font: string;
};

const PrimaryButton: React.FC<Props> = ({
    text,
    htmlType,
    height = '70px',
    width = '360px',
    font = '24px',
    onClick,
}: Props) => (
    <Button
        type={htmlType || 'button'}
        onClick={onClick}
        className="primary-button"
        height={height}
        width={width}
        font={font}>
        {text || ''}
    </Button>
);

PrimaryButton.defaultProps = {
    text: '',
    htmlType: 'button',
    onClick: () => {},
};

const Button = styled.button<StyleProps>`
    background-color: ${({ theme }) => theme.darkBlue};
    color: #ffff;
    width: ${props => props.width};
    font-family: Gudea;
    font-style: normal;
    font-weight: normal;
    font-size: ${props => props.font};
    line-height: 30px;
    display: flex;
    align-items: center;
    text-align: center;
    text-transform: uppercase;
    height: ${props => props.height};
    outline: none;
    justify-content: center;

    &:hover {
        background-color: ${({ theme }) => theme.lightBlue};
    }
`;

export default PrimaryButton;
