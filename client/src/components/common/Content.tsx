import React, { ReactChildren, ReactChild } from 'react';
import styled from 'styled-components';

type Props = {
    children: ReactChild | ReactChildren | ReactChild[] | ReactChildren[];
};

const Content: React.FC<Props> = ({ children }: Props) => <StyledContent className="content">{children}</StyledContent>;

const StyledContent = styled.div`
    background-color: ${({ theme }) => theme.lightBlue};
    flex: 1;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export default Content;
