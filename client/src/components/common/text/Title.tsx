import React from 'react';
import styled from 'styled-components';

type Props = {
    level?: 1 | 2 | 3;
    children?: React.ReactChild | React.ReactChild[];
    style?: React.CSSProperties;
};

const H1 = styled.h1`
    font-size: 4em;
    font-family: 'Gudea';
    color: white !important;
    font-weight: bold !important;
`;
const H2 = styled.h2`
    font-size: 2em;
`;
const H3 = styled.h3`
    font-size: 1.1em;
`;

const Title: React.FC<Props> = ({ level = 1, children, style }: Props) => {
    const getLevel = () => {
        switch (level) {
            case 1:
                return <H1 style={style}>{children}</H1>;
            case 2:
                return <H2 style={style}>{children}</H2>;
            case 3:
                return <H3 style={style}>{children}</H3>;

            default:
                return <>{children}</>;
        }
    };
    return getLevel();
};

Title.defaultProps = {
    level: 1,
    children: <></>,
};

export default Title;
