import React, { useState, useEffect } from 'react';
import * as dal from '@/dal';
import { Card, Select, Spin } from 'antd';

import styled from 'styled-components';
import { groupBy } from 'lodash';
import dayjs from 'dayjs';
import { Line, Bar } from '@/components/common/Nivo';

const { Option } = Select;

const StyledCard = styled(Card)`
    height: 80%;
    width: 80%;
    align-self: center;

    .ant-card-body {
        height: 100%;
        width: 100%;
    }
`;

const BrowserAudit: React.VFC = () => {
    const [audit, setAudit] = useState<IVisitorAudit[]>([]);
    const [filter, setFilter] = useState('activity');
    const [loading, setLoading] = useState(false);
    const [lineDate, setLineDate] = useState<{ type: 'line' | 'bar' | undefined; data: any[] }>({
        data: [],
        type: undefined,
    });

    const handleData = () => {
        switch (filter) {
            case 'activity':
                setLoading(true);
                const groupedAudit = groupBy(
                    audit.map(a => ({ ...a, createdAt: dayjs(a.createdAt).format('YYYY-MM-DD HH') })),
                    'createdAt'
                );
                const data = Object.entries(groupedAudit).map(k => ({ x: k[0], y: k[1].length }));
                setLineDate({
                    type: 'line',
                    data: [
                        {
                            id: 'activity',
                            color: 'hsl(172, 70%, 50%)',
                            data,
                        },
                    ],
                });
                setLoading(false);
                break;
            case 'resolutionX':
                const groupedResolutionX = groupBy(audit, 'resolutionX');
                const resXData = Object.entries(groupedResolutionX).map(k => ({ x: k[0], y: k[1].length }));
                setLineDate({
                    type: 'bar',
                    data: resXData,
                });

                break;
            case 'resolutionY':
                const groupedResolutionY = groupBy(audit, 'resolutionY');
                const resYData = Object.entries(groupedResolutionY).map(k => ({ x: k[0], y: k[1].length }));
                setLineDate({
                    type: 'bar',
                    data: resYData,
                });
                break;

            case 'browserName':
                const groupedName = groupBy(audit, 'browserName');
                const browserNameData = Object.entries(groupedName).map(k => ({ x: k[0], y: k[1].length }));
                setLineDate({
                    type: 'bar',
                    data: browserNameData,
                });
                break;

            case 'os':
                const groupedOS = groupBy(audit, 'oS');
                const browserOSData = Object.entries(groupedOS).map(k => ({ x: k[0], y: k[1].length }));
                setLineDate({
                    type: 'bar',
                    data: browserOSData,
                });
                break;

            default:
                break;
        }
    };

    useEffect(() => {
        const fetchAudit = async () => {
            setLoading(true);
            const { data } = await dal.audit.getAuditBrowserDetails().finally(() => setLoading(false));
            setAudit(data);
        };
        fetchAudit();
    }, []);

    useEffect(() => {
        handleData();
    }, [filter]);

    const displayPlot = () => {
        const { data, type } = lineDate;

        switch (type) {
            case 'bar':
                return <Bar data={data} keys={['y']} indexBy="x" />;
            case 'line':
                return <Line data={data} />;
            default:
                return <> Choose category</>;
        }
    };

    return (
        <StyledCard
            title={
                <Select defaultValue="default" style={{ width: 200 }} onChange={e => setFilter(e as string)}>
                    <Option value="default">Choose category</Option>
                    <Option value="activity">Activity</Option>
                    <Option value="resolutionX">Resolution X</Option>
                    <Option value="resolutionY">Resolution Y</Option>
                    <Option value="browserName">Browser</Option>
                    <Option value="os">OS</Option>
                </Select>
            }>
            <>{loading ? <Spin spinning={loading} /> : displayPlot()}</>
        </StyledCard>
    );
};

export default BrowserAudit;
