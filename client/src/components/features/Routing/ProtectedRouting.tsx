import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';

type Props = {
    component: React.ElementType;
    redirectPath?: string;
    open: boolean;
} & RouteProps;

const ProtectedRouting = ({ component: Component, redirectPath = '/', open, ...rest }: Props) => (
    <Route
        {...rest}
        render={props => {
            if (open) {
                return <Component {...rest} {...props} />;
            }
            return (
                <Redirect
                    to={{
                        pathname: redirectPath,
                        state: {
                            from: props.location,
                        },
                    }}
                />
            );
        }}
    />
);

export default ProtectedRouting;
