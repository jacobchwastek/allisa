import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
    flex-wrap: wrap;
    justify-content: space-evenly;
`;

const PricingPlanGroupContainer: React.FC = ({ children }) => (
    <Container className="pricing-container">{children}</Container>
);

export default PricingPlanGroupContainer;
