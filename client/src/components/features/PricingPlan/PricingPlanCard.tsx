import React from 'react';
import { Card } from 'antd';
import styled from 'styled-components';
import { button } from '@/components/common';

const { Meta } = Card;
const { PrimaryButton } = button;

type Props = {
    src: string;
    price: number;
    onChoose: () => void;
};

const StyledCard = styled(Card)`
    max-width: 500px;
    margin: 10px 6px;
    min-width: 300px;
    padding: 1% 0;
    .ant-card-cover {
        display: flex;
        justify-content: center;

        img {
            width: auto;
        }
    }
    .ant-card-meta {
        text-align: center;
    }

    .ant-card-body {
        display: flex;
        flex-direction: column;
    }
`;

const PricingPlanCard: React.FC<Props> = ({ src, price, onChoose }: Props) => (
    <StyledCard hoverable cover={<img alt={src} src={src} />}>
        <Meta
            title={`$ ${price} /month`}
            description={
                <div>
                    <ul>
                        <li>xd</li>
                        <li>xd</li>
                        <li>xd</li>
                        <li>xd</li>
                    </ul>
                </div>
            }
        />
        <PrimaryButton onClick={onChoose} font="16px" width="100%" height="40px" text="Subscribe now" />
    </StyledCard>
);

export default PricingPlanCard;
