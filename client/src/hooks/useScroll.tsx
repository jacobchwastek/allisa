import React, { useEffect } from 'react';

export const useScroll = (callback: any) => {
    useEffect(() => {
        window.addEventListener('scroll', callback);
        return () => window.removeEventListener('scroll', callback);
    }, [callback]);
};
