import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

type Props = {
    locations: string[];
};

const useShouldDisplay = ({ locations }: Props) => {
    const [shouldDisplay, setShouldDisplay] = useState(false);
    const location = useLocation();
    useEffect(() => {
        const { pathname } = location;
        let tempShouldDisplay = false;
        locations.forEach(loc => {
            if (pathname === loc || loc === pathname.substring(0, pathname.lastIndexOf('/'))) {
                tempShouldDisplay = true;
            }
        });
        setShouldDisplay(tempShouldDisplay);
    }, [location, locations]);

    return { shouldDisplay };
};

export default useShouldDisplay;
