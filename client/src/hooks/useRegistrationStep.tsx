import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

type Props = {
    user: IUser;
};

const useRegistrationStep = ({ user }: Props) => {
    const history = useHistory();

    useEffect(() => {
        const handleRegistrationStep = () => {
            const {
                accountSettings: { registrationStep },
                role,
            } = user;

            if (role === 'admin') return;
            switch (registrationStep) {
                case 0:
                    break;
                case 1:
                    history.push('/confirm-email');
                    break;
                case 2:
                    history.push('/choose-plan');
                    break;
                case 3:
                    history.push('/');
                    break;
                default:
                    break;
            }
        };
        handleRegistrationStep();
    }, [user]);
};

export default useRegistrationStep;
