import React, { useEffect } from 'react';

export const useMouseUp = (callback: any) => {
    useEffect(() => {
        window.addEventListener('mouseup', callback);
        return () => window.removeEventListener('mouseup', callback);
    }, [callback]);
};
