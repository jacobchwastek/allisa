import { DefaultTheme } from 'styled-components';

export const defaultTheme: DefaultTheme = {
    darkBlue: '#1D3557',
    lightBlue: '#457B9D',
    lightGreen: '#ABC4AB',
    light: '#f3f7f0',
    white: '#FFFFFF',
    red: '#e76f51',
};
