import {
  Injectable,
  UnauthorizedException,
  HttpException,
  HttpStatus,
  Inject,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { AccountSettings, User } from '../model';
import { compare } from 'bcrypt';
import { CreateUserDto } from '../dto/create-user.dto';
import { LoginUserDto } from '../dto/login-user.dto';
import { AccountSettingsService } from 'src/account-settings/account-settings.service';

@Injectable()
export class AuthService {
  constructor(
    @Inject('AccountSettingsService')
    private accountSettingsService: AccountSettingsService,
    private jwtService: JwtService,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(AccountSettings)
    private accountSettingsRepository: Repository<AccountSettings>,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersRepository.findOne({
      where: { email },
      relations: ['accountSettings'],
    });

    if (user && (await compare(password, user.password))) {
      return user;
    }

    throw new UnauthorizedException();
  }

  async register({ email, password }: CreateUserDto) {
    const existingUser = await this.usersRepository.findOne({
      where: { email },
    });

    if (existingUser)
      throw new HttpException('User already exists', HttpStatus.CONFLICT);

    const user = this.usersRepository.create({ password, email });

    const systemUser = await this.usersRepository.save(user);

    const accountSettings = await this.accountSettingsRepository.save({
      registrationStep: 1,
      user: systemUser,
    });

    const { token } = await this.accountSettingsService.createEmailConfirmation(
      systemUser.id,
    );

    await this.usersRepository.save({ ...systemUser, accountSettings });

    await this.accountSettingsService.sendCofirmationEmail(email, token);

    return systemUser;
  }

  async login({ id }: LoginUserDto) {
    const payload = { id };
    const token = this.jwtService.sign(payload);
    return `Authentication=${token}; HttpOnly; Path=/; Max-Age=${process.env.JWT_EXPIRATION_TIME}`;
  }

  async logout() {
    return `Authentication=; HttpOnly; Path=/; Max-Age=0`;
  }
}
