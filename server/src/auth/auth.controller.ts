import { Controller, Req, Res, Get, Post, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Public } from 'src/decorators/public.decorator';
import { Request, Response } from 'express';
import { LocalAuthenticationGuard } from './local-authentication.guard';
import { User } from 'src/model/user.entity';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @UseGuards(LocalAuthenticationGuard)
  @Post('login')
  async login(@Req() req, @Res() res: Response) {
    const { user } = req;
    const cookie = await this.authService.login(user);
    res.setHeader('Set-Cookie', cookie);
    user.password = undefined;
    return res.status(200).json({
      status: 'success',
      data: user,
    });
  }

  @Public()
  @Post('register')
  async register(@Req() req: Request) {
    const {
      body: { email, password },
    } = req;

    return this.authService.register({ email, password });
  }

  @Post('logout')
  async logOut(@Req() _, @Res() response: Response) {
    const cookie = await this.authService.logout();
    response.setHeader('Set-Cookie', cookie);
    return response.sendStatus(200);
  }

  @Get()
  authenticate(@Req() req: Request) {
    const user = req.user as User;
    user.password = undefined;
    return user;
  }

  @Get('/test')
  test(@Req() _, @Res() response: Response) {
    return response.sendStatus(401);
  }
}
