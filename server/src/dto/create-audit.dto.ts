export class CreateVisitorAudit {
  resolutionX: number;
  resolutionY: number;
  name: string;
  version: string;
  os: string;
}
