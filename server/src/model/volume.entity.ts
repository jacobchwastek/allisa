import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'volumes' })
export class Volume implements IVolume {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'path_name' })
  pathName: string;

  @Column({ name: 'host_name' })
  hostName: string;

  @Column({ name: 'username' })
  userName: string;

  @Column({ name: 'password' })
  password: string;
}
