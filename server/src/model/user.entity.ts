import {
  Entity,
  Column,
  OneToOne,
  PrimaryGeneratedColumn,
  BeforeInsert,
  OneToMany,
} from 'typeorm';
import { hash } from 'bcrypt';
import { VisitorAudit } from './visitor-audit.entity';

@Entity({ name: 'users' })
export class User implements IUser {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'first_name', nullable: true, default: null })
  firstName: string;

  @Column({ name: 'last_name', nullable: true, default: null })
  lastName: string;

  @Column({ name: 'email', nullable: false })
  email: string;

  @Column({ name: 'password', nullable: false })
  password: string;

  @Column({ name: 'role', nullable: false, default: 'user' })
  role: string;

  @BeforeInsert() async hashPassword() {
    this.password = await hash(this.password, 10);
  }

  @OneToOne('AccountSettings', 'user')
  // @JoinColumn()
  accountSettings: IAccountSettings;

  @OneToOne('Subscription', 'user')
  // @JoinColumn()
  subscription: ISubscription;

  @OneToOne('ResetPasswordRequest', 'user')
  // @JoinColumn()
  resetPasswordRequest: any;

  @OneToMany('VisitorAudit', 'user')
  visitorAudit: VisitorAudit;
}
