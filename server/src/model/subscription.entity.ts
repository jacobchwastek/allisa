import {
  Entity,
  Column,
  OneToOne,
  JoinColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('subscriptions')
export class Subscription {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'subscription_id' })
  subscriptionId: string;

  @Column({ name: 'created_at' })
  createdAt: Date;

  @OneToOne('User', 'subscription')
  @JoinColumn()
  user: IUser;

  @Column()
  currency: string;

  @Column()
  amount: number;

  @Column({ name: 'period_start', nullable: true })
  periodStart?: Date;

  @Column({ name: 'period_end', nullable: true })
  persiodEnd?: Date;
}
