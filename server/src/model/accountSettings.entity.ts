import {
  Entity,
  Column,
  OneToOne,
  JoinColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';

@Entity({ name: 'account_settings' })
export class AccountSettings {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'registration_step' })
  registrationStep: number;

  @OneToMany('EmailConfirmation', 'emailConfirmations')
  emailConfirmations: IEmailConfirmation[];

  @OneToOne('User', 'accountSettings')
  @JoinColumn()
  user: IUser;
}
