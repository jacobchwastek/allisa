import {
  Entity,
  Column,
  ManyToOne,
  JoinColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'visitors_audit' })
export class VisitorAudit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'resolution_x' })
  resolutionX: number;

  @Column({ name: 'resolution_y' })
  resolutionY: number;

  @Column({ name: 'browser_name' })
  browserName: string;

  @Column({ name: 'browser_version' })
  browserVersion: string;

  @Column({ name: 'os' })
  oS: string;

  @Column({ name: 'created_at' })
  createdAt: Date;

  @ManyToOne('User', 'VisitorAudit')
  @JoinColumn({ name: 'user_id' })
  user: IUser;
}
