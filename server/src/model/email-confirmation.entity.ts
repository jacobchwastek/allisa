import {
  Entity,
  Column,
  JoinColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';

@Entity({ name: 'email_confirmations' })
export class EmailConfirmation implements IEmailConfirmation {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  token: string;

  @Column()
  email: string;

  @Column({ name: 'created_at' })
  createdAt: Date;

  @Column()
  expired: boolean;

  @Column({ name: 'is_confirmed' })
  isConfirmed: boolean;

  @ManyToOne('AccountSettings', 'emailConfirmations')
  @JoinColumn({ name: 'account_setting' })
  accountSettings: IAccountSettings;
}
