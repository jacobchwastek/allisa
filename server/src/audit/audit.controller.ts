import { Controller, Req, Res, Post, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import { Response, Request } from 'express';
import { VisitorAudit } from '../model/visitor-audit.entity';
import { CreateVisitorAudit } from '../dto/create-audit.dto';
import { Repository } from 'typeorm';
import { v4 } from 'uuid';

@ApiTags('audit')
@Controller('audit')
export class AuditController {
  constructor(
    @InjectRepository(VisitorAudit)
    private visitorAuditRepository: Repository<VisitorAudit>,
  ) {}

  @Post('browser')
  async AuditBrowserDetails(@Req() req: Request, @Res() res: Response) {
    const {
      name,
      version,
      os,
      resolutionX,
      resolutionY,
    } = req.body as CreateVisitorAudit;
    const user = req.user as IUser;

    if (!user.id) return res.sendStatus(304);

    await this.visitorAuditRepository.save({
      id: v4(),
      browserName: name,
      browserVersion: version,
      oS: os,
      resolutionX,
      resolutionY,
      createdAt: new Date(),
      user: { id: user.id },
    });

    return res.sendStatus(200);
  }

  @Get('browser')
  async GetBrowserAudit(@Req() req: Request, @Res() res: Response) {
    const user = req.user as IUser;

    if (!user.id) return res.sendStatus(304);

    const audit = await this.visitorAuditRepository.find();
    return res.status(200).json(audit);
  }
}
