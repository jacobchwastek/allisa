import { Module } from '@nestjs/common';
import { AuditController } from './audit.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VisitorAudit } from '../model/visitor-audit.entity';

@Module({
  controllers: [AuditController],
  imports: [TypeOrmModule.forFeature([VisitorAudit])],
})
export class AuditModule {}
