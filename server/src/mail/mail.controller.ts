import {
  Controller,
  Inject,
  Injectable,
  Next,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { Public } from 'src/decorators/public.decorator';
import { MailService } from './mail.service';
import { User, AccountSettings, EmailConfirmation } from '../model';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
@Controller('mail')
export class MailController {
  constructor(
    @Inject(MailService)
    private mailService: MailService,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(AccountSettings)
    private accountSettingsRepository: Repository<AccountSettings>,
    @InjectRepository(EmailConfirmation)
    private emailConfirmationRepository: Repository<EmailConfirmation>,
  ) {}
  @Public()
  @Post('test')
  async register(
    @Req() _: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    try {
      const msg = {
        from: 'jacobchwastek@gmail.com', // Change to your recipient
        to: 'jacobchwastek+user@gmail.com', // Change to your verified sender
        subject: 'Sending with SendGrid is Fun',
        text: 'and easy to do anywhere, even with Node.js',
        html: '<strong>and easy to do anywhere, even with Node.js</strong>',
      };
      await this.mailService.sendMail(msg);
      res.sendStatus(200);
    } catch (error) {
      next(error);
    }
  }

  @Public()
  @Post('confirm')
  async confirm(@Req() req: Request, @Res() res: Response) {
    const {
      body: { token },
    } = req;

    if (!token) return res.sendStatus(404);

    const emailConfirmation = await this.emailConfirmationRepository.findOne({
      where: { token: token },
      relations: ['accountSettings'],
    });

    if (!emailConfirmation)
      return res.status(404).send({ message: 'Invalid confirmation token' });

    if (emailConfirmation.isConfirmed)
      return res.status(400).send({ message: 'Email already confirmed' });

    if (emailConfirmation.expired)
      return res.status(400).send({ message: 'Email link is expired' });

    emailConfirmation.isConfirmed = true;
    emailConfirmation.expired = true;
    const accountSettings = emailConfirmation.accountSettings;

    accountSettings.registrationStep = 2;

    await this.emailConfirmationRepository.save(emailConfirmation);
    await this.accountSettingsRepository.save(accountSettings);

    return res.sendStatus(200);
  }
}
