import { Module } from '@nestjs/common';
import { MailController } from './mail.controller';
import { MailService } from './mail.service';
import { SendGridModule } from '@ntegral/nestjs-sendgrid';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountSettings, EmailConfirmation, User } from 'src/model';

@Module({
  controllers: [MailController],
  providers: [MailService],
  imports: [
    SendGridModule.forRoot({
      apiKey: process.env.SENDGRID_API_KEY,
    }),
    TypeOrmModule.forFeature([User, AccountSettings, EmailConfirmation]),
  ],
})
export class MailModule {}
