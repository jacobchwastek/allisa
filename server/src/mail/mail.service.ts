import { Injectable } from '@nestjs/common';
import { InjectSendGrid, SendGridService } from '@ntegral/nestjs-sendgrid';
import { MailDataRequired } from '@sendgrid/mail';

@Injectable()
export class MailService {
  public constructor(
    @InjectSendGrid() private readonly client: SendGridService,
  ) {}

  async sendMail(message: TestMessage) {
    await this.client.send(message);
  }

  async sendConfirmationEmail(toEmail: string, url: string) {
    const templateId = 'd-4e484ab930e94a88810a3147b9771fa6';

    const message: Partial<MailDataRequired> | Partial<MailDataRequired>[] = {
      templateId,
      from: process.env.SENDGRID_SENDER_EMAIL,
      to: toEmail,
      dynamicTemplateData: {
        URL: url,
      },
    };

    await this.client.send(message);
  }
}
