interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  role: string;
}

interface IAccountSettings {
  id: string;
  user: IUser;
  registrationStep: number;
  emailConfirmations: IEmailConfirmation[];
}

interface IVolume {
  id: string;
  pathName: string;
  hostName: string;
  userName: string;
  password: string;
}

interface ISubscription {
  id: string;
  subscriptionId: string;
  createdAt: Date;
  user: IUser;
  currency: string;
  amount: number;
  periodStart?: Date;
  persiodEnd?: Date;
}

interface IEmailConfirmation {
  id: string;
  token: string;
  email: string;
  createdAt: Date;
  expired: boolean;
  isConfirmed: boolean;
}
