import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AccountSettings, EmailConfirmation } from '../model';
import { v4 as uuid } from 'uuid';
import { MailService } from 'src/mail/mail.service';

@Injectable()
export class AccountSettingsService {
  constructor(
    @Inject(MailService)
    private mailService: MailService,
    @InjectRepository(AccountSettings)
    private accountSettingsRepository: Repository<AccountSettings>,
    @InjectRepository(EmailConfirmation)
    private emailConfirmationRepository: Repository<EmailConfirmation>,
  ) {}

  async createEmailConfirmation(userId: string) {
    const accountSetting = await this.accountSettingsRepository.findOne({
      where: { user: { id: userId } },
      relations: ['user'],
    });

    return await this.emailConfirmationRepository.save({
      id: uuid(),
      createdAt: new Date(),
      email: accountSetting.user.email,
      expired: false,
      isConfirmed: false,
      token: uuid(),
      accountSettings: accountSetting,
    });
  }

  async sendCofirmationEmail(email: string, token: string) {
    const returnUrl = `${process.env.FRONTEND_URL}/confirm?token=${token}`;

    await this.mailService.sendConfirmationEmail(email, returnUrl);
  }
}
