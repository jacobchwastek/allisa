import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailService } from 'src/mail/mail.service';
import { AccountSettings, EmailConfirmation } from 'src/model';
import { AccountSettingsService } from './account-settings.service';

@Module({
  providers: [AccountSettingsService, MailService],
  imports: [TypeOrmModule.forFeature([AccountSettings, EmailConfirmation])],
})
export class AccountSettingsModule {}
