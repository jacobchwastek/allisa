/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { join, resolve } from 'path';
const fileStreamRotor = require('file-stream-rotator');

const morganStream = {
  stream: fileStreamRotor.getStream({
    filename: join(resolve(), '/logs/access.log'),
    frequency: 'daily',
    verbose: false,
    date_format: 'YYYY-MM-DD',
  }),
};

export { morganStream };
